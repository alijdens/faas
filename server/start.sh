#!/usr/bin/env bash

export DJANGO_SUPERUSER_PASSWORD=root

python manage.py migrate
python manage.py createsuperuser --noinput --username root --email test@test.com
#python manage.py grpcrunserver --dev
python manage.py runserver 0.0.0.0:8000