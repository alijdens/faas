from django.forms import ModelForm, Textarea
from django.contrib import admin
from .models import FirewallConfig

admin.site.site_header = 'FaaS Administration'


class FirewallConfigForm(ModelForm):
    class Meta:
        model = FirewallConfig
        fields = ('__all__')
        widgets = {
            'command': Textarea(attrs={'cols': 120, 'rows': 10}),
        }

admin.site.register(
    FirewallConfig,
    form=FirewallConfigForm,
    list_display=("config_group", "version")
)
