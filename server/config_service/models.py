from django.db import models
from . import events


class FirewallConfig(models.Model):
    config_group = models.CharField(
        primary_key=True,
        max_length=200,
        help_text='Indica el grupo de equipos que van a recibir esta configuración.\n'
                  'De esta forma se pueden tener distintos grupos con distintas configuraciones.'
    )
    version = models.IntegerField(
        help_text='numero de version que se incrementa en cada actualización de la config',
        editable=False
    )

    command = models.CharField(
        max_length=300,
        help_text='La configuracion a ejecutar en los equipos clientes.',
        null=True
    )

    indexes = [
        models.Index(fields=['config_group', 'version']),
    ]

    def save(self, *args, **kw):
        if self.version is None:
            self.version = 0
        self.version += 1

        super().save(*args, **kw)

        # notify listeners about a new version available
        events.notify(self)
