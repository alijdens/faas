from queue import Queue
from django.http import JsonResponse
from . import events
from . import serializers
from .models import FirewallConfig


def get_latest_config(request, group: str, version: int):
    """Returns the latest configuration version in JSON format.
    If the client already hasWaits for a configuration update"""

    # checks if there is a newer version than the one the client has
    latest_config = _get_latest_config(group=group)
    if latest_config is None:
        return JsonResponse({'error': f"group '{group}' not found"}, status=404)

    if latest_config.version >= version:
        # returns the latest version
        return _send_config(latest_config)

    # at this point, the client has the latest config, so we wait until a new
    # one is created to respond
    q = Queue()
    events.register(group, q)
    try:
        print("waiting")
        new_config = q.get()
        return _send_config(new_config)
    finally:
        events.unregister(group, q)


def _get_latest_config(group):
    return FirewallConfig.objects.filter(config_group=group).order_by('-version').first()


def _send_config(new_config):
    serializer = serializers.FirewallConfigSerializer(instance=new_config)
    return JsonResponse(serializer.data, safe=False)
