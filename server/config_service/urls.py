from django.urls import path

from . import views

urlpatterns = [
    path('<str:group>/version/<int:version>', views.get_latest_config, name='get_latest_config'),
]