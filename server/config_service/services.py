from queue import Queue

from .models import FirewallConfig
from django_grpc_framework import generics
from .serializers import FirewallConfigProtoSerializer
from django.db.models import signals
from . import events


class FirewallConfigService(generics.ModelService):
    """gRPC service that allows config synchronization."""

    queryset = FirewallConfig.objects.filter(pk=1)
    serializer_class = FirewallConfigProtoSerializer

    def Connect(self, request, context):
        q = Queue()
        events.register(q)

        try:
            print("receiving...")
            while True:
                new_config = q.get()
                print("sending new config...")
                serializer = FirewallConfigProtoSerializer(new_config)
                yield serializer.message
        finally:
            events.unregister(q)