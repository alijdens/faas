from django_grpc_framework import proto_serializers
from rest_framework import serializers
from proto import service_pb2
from .models import FirewallConfig


class FirewallConfigProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = FirewallConfig
        proto_class = service_pb2.Config
        fields = ['__all__']


class FirewallConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = FirewallConfig
        fields = "__all__"
