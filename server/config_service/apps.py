from django.apps import AppConfig


class ConfigServiceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'config_service'
