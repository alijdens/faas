from collections import defaultdict
from django.db.models import query_utils, signals


_receivers = defaultdict(list)

def register(group, listener):
    """Registers a new listener waiting for a config change event."""

    print("registering", listener, "in group", group)
    _receivers[group].append(listener)


def unregister(group, listener):
    """Unregisters a new listener waiting for a config change event."""

    print("unregistering", listener, "from group", group)
    _receivers[group].remove(listener)


def notify(new_config):
    """Notifies all listeners in a certain group about a new version."""

    group = new_config.config_group
    receivers = _receivers[group]

    print("new event, dispatch to", len(receivers), "receivers in group", group)
    for receiver in receivers:
        print("dispatch to", receiver)
        receiver.put(new_config)
