# FaaS
TP para la materia Criptografía y Seguridad informática.


## Setup Local
El proyecto usa Python 3.9.1. Para desarrollo local, es necesario instalarse esa versión (una más nueva debería funcionar también) y crear un virtual env:

```shell
python3 -m venv .venv
source .venv/bin/activate
python3 install -r requirements.txt
```

## Setup Docker (recomendado)
Para ejecutar la aplicación con Docker es necesario tener instalado [Docker](https://docs.docker.com/get-docker/).

Luego se debe ejecutar:

```shell
docker compose build  # solo necesario la primera vez, o luego de realizar algún cambio que requiera regenerar la imagen
docker compose up
```

## Backoffice
Una vez levantado `docker compose` se puede acceder al backoffice entrando a `http://localhost:8000/admin/` ingresando `root` como usuario y contraseña.
Para acceder al ABM de configuraciones se puede clickear en el link [Firewall configs](http://localhost:8000/admin/config_service/firewallconfig/).

## API
Para descargar la configuración se debe enviar un request a `/config/<grupo>/version/<version>`, donde `<grupo>` es el nombre del group asignado desde el backoffice y `<version>` es el número de version que se desea descargar.

Por ejemplo, un nuevo cliente que se conecta por primer vez y desea la configuración de un grupo llamado `default` puede comenzar solicitando la versión 0: `/config/default/version/0` y el servidor le responderá enviando la última versión disponible para `default`. Dentro de la respuesta va a haber un campo llamado `version` que indica la versión enviada por el servidor (no necesariamente la indicada por el cliente).

Suponiendo que la versión enviada por el servidor fuera 9, en su siguiente request el cliente debe indicar que desea la versión 10. En este caso, el servidor no responderá hasta que se cree la versión 10 desde el backoffice.

Un ejemplo sobre como usar la API se puede ver en [`client/api_example.py`](client/api_example.py)

## Agregar campos a la configuración
Para agregar nuevos campos a la configuración hay que modificar la clase `FirewallConfig` en el archivo [`server/config_service/models.py`](server/config_service/models.py). Al agregar nuevos campos a esa clase aparecen automáticamente en el backoffice y la descarga de configuración.

Al modificar el modelo `FirewallConfig` es necesario crear una migración para actualizar la base de datos. Para esto, basta ejecutar `make migrations` que debería crear un nuevo archivo en `server/config_service/migrations` con los cambios. Para aplicar la migración (y hacer efectivo el cambio) se puede ejecutar `make migrate` o reiniciar `docker compose` (opcionalmente borrando la DB).

### Borrado de la DB
Para borrar la DB es necesario primero eliminar los contenedores de la aplicación ejecutando `docker compose down` y luego borrar el volumen ejecutando `docker volume rm faas_pgdata`.
