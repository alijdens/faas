"""
Este es un ejemplo de como consultar la configuración y mantenerla sincronizada con el servidor.
Es necesario tener docker compose inicializado y escuchando en el puerto 8000. Tambien es necesario
ingresar al backoffice (http://localhost:8000/admin/config_service/firewallconfig/add/) y crear
el grupo de configuraciones llamado "default".

Inicialmente el cliente va a mostrar la última versión de la configuración y luego se queda
esperando actualizaciones. Probar entrar al backoffice, abrir el grupo default y apretar el
botón `save` (no es necesario modificar el config group) para ver como el cliente
autommáticamente muestra la nueva configuración y espera el siguiente cambio.
"""

import requests
import os
import time
import logging

server_address = os.environ["SERVER_ADDRESS"]
server_listening_port = os.environ["SERVER_LISTENING_PORT"]
group = os.environ["FIREWALL_GROUP"]
version = 1

logging.basicConfig(level=logging.DEBUG)


while True:
    try:
        url = f'http://{server_address}:{server_listening_port}/config/{group}/version/{version}'
        r = requests.get(url)
        assert r.ok, r.text

        config = r.json()
        logging.info(config)

        # toma el comando y lo ejecuta
        if config["command"]:
            commands = config["command"].strip().replace('\r', '').split('\n')
            for command in commands:
                logging.info(command)
                os.system(command)

        # actualiza el numero de version a pedir a continuación
        version = config['version'] + 1
    except Exception as e:
        logging.exception(e)
        time.sleep(5)
